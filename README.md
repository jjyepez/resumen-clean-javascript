# Clean Javascript

## Deuda Técnica
> (acá lo más relevante o interesante para ti)

## SECCIÓN I: CLEAN CODE

###  ¿Qué es Clean Code?
> (acá lo más relevante o interesante para ti)
###  Variables, nombres y ámbito
> (acá lo más relevante o interesante para ti)
###  Funciones
> (acá lo más relevante o interesante para ti)
###  Clases
> (acá lo más relevante o interesante para ti)
###  Comentarios y formato
> (acá lo más relevante o interesante para ti)

## SECCIÓN II: PRINCIPIOS SOLID

### De STUPID a SOLID
> (acá lo más relevante o interesante para ti)

### Principios SOLID al rescate
> (acá lo más relevante o interesante para ti)

- SRP - Principio de responsabilidad única
> (acá lo más relevante o interesante para ti)

-    OCP - Principio Abierto/Cerrado
> (acá lo más relevante o interesante para ti)

-    LSP - Principio de sustitución de Liskov
> (acá lo más relevante o interesante para ti)

-    ISP - Principio de segregación de la interfaz
> (acá lo más relevante o interesante para ti)

-    DIP - Principio de inversión de dependenci
> (acá lo más relevante o interesante para ti)
